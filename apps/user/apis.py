import random
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView, Response

from .models import User
from apps.common.apis import CustomViewSet
from .serializers import ProfileSerializer


ITEMS = ['MotoTracker', 'CarTracker', 'AssetLock', 'EagleCam']
DISTRICTS = ['Dhaka', 'Chittagong', 'Rangpur', 'Sylhet']
WORK_AREA = ['Logistics', 'Delivery', 'FMCG', 'Telecom', 'Sales & Distribution']
DATES = [
    '2020-07-01', '2020-07-02', '2020-07-03',
    '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07',

    '2020-08-01', '2020-08-02', '2020-08-03',
    '2020-08-04', '2020-08-05', '2020-08-06', '2020-08-07',
]

FS = ['MD.', 'Sheikh', 'Syed', 'AKM', 'Mohammad', 'Khondokar', 'ABM']
LS = [
    'Ashraful', 'Salehin', 'Musa', 'Kayyum',
    'Shmim Hasan', 'Rafayet Khan', 'Belal hussain', 'Anisul Haque', 'Muhit', 'Abdur Rahman',
    'Khairul Bashar', 'Rafiqul Islam', 'Ishtiyak ahmed', 'Mustak Ahamed',
    'Rafiul Amin', 'Maidul Islam', 'Nafisul Islam', 'Ashikur Rahman', 'Belayet Hussain',
    'Rounak Hasan', 'Aminul Islam', 'Ismail Hussain', 'Rashidul Haque', 'Omar Hisham'
]

def get_name():
    return FS[random.randint(0, len(FS)-1)] + ' ' + LS[random.randint(0, len(LS)-1)]

class ProfileViewSet(CustomViewSet):
    """
    retrieve:
        Return single object

        Response:
        ---
            {
                'id': 12,
                'username': 'username',
                'name': 'name1',
                'phone': '+88017XXXXXXXX',
                'image': 'url,
                'email': 'manager.email',
                'designation': 'MD'
            }

    list:
        Return object list

        Query parameters:
        ---
            role:
                type: int
                required: No
                choices: (1, type1), (2, type2), (0, type3)

            token:
                type: str
                required: No

        Sample Response:
        ---

            [
                {
                    //profile details
                },..
            ]

    create:
        Create object

        Sample Submit:
        ---
            {
                'username': 'user1',
                'full_name': 'name1',
                'phone': '+88017XXXXXXXX',
                'image': 'url',
                'email': 'manager.email',
                'designation': 'MD'
            }

    partial_update:
        Update single object

        Sample Submit:
        ---
            {
                // same as submit
            }

    paginated:
        Return paginated object list

        Sample response:
        ---
            {
                'limit': 10,
                'offset': 20,
                'count': 101,
                'next': 'limit=5&offset=30',
                'prev': 'limit=5&offset=10',
                'results': [
                    {
                        //same as details
                    },...
                ]
            }
    """
    permission_classes = (IsAuthenticated,)
    ObjModel = User
    ObjSerializer = ProfileSerializer


class DataAPIView(APIView):
    permission_classes = (AllowAny, )

    def get(self, request, format=None):

        data = []
        for i in range(60):
            item = {
                'date': DATES[random.randint(0, len(DATES)-1)],
                'customer_name': get_name(),
                'product': ITEMS[random.randint(0, len(ITEMS)-1)],
                'district': DISTRICTS[random.randint(0, len(DISTRICTS)-1)],
                'customer_work_area': WORK_AREA[random.randint(0, len(WORK_AREA)-1)],
                'order_quantity': random.randint(1, 10)
            }
            data.append(item)
        return Response(data, status=200)
